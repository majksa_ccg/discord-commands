package cz.majksa.discord.commands;

import cz.majksa.discord.commands.exception.AnnotationNotFoundException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

@Setter
@Getter
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class CommandsBuilder {
    static final Logger LOGGER = Logger.getLogger("Discord Commands Sent");

    private final CommandContainer commands = new CommandContainer();
    private final MiddlewareContainer middlewares = new MiddlewareContainer();
    private final Help help;
    private final String prefix;

    public static CommandsBuilder from(@NotNull Help help, @NotNull String prefix) {
        return new CommandsBuilder(help, prefix);
    }

    /**
     * This method will register provided commands.
     *
     * @param commands {@link AbstractCommand} list of commands to be registered
     */
    public @NotNull CommandsBuilder registerCommands(@NotNull AbstractCommand... commands) throws AnnotationNotFoundException {
        this.commands.add(commands);
        return this;
    }

    /**
     * Adds {@link IMiddleware} to specified position.
     * Throws {@link IllegalArgumentException} if position is already occupied.
     *
     * @param position Position of middleware
     * @param middleware {@link IMiddleware}
     * @return {@link MiddlewareContainer}
     */
    public @NotNull CommandsBuilder registerMiddleware(int position, @NotNull IMiddleware middleware) {
        this.middlewares.register(position, middleware);
        return this;
    }

    public void build(@NotNull JDABuilder builder) {
        builder.addEventListeners(new MessageReceivedListener(prefix, prepare()));
    }

    public void build(@NotNull JDA jda) {
        jda.addEventListener(new MessageReceivedListener(prefix, prepare()));
    }

    protected CommandManager prepare() {
        CommandManager commandManager = new CommandManager(commands, middlewares, help);
        help.setCommands(commands);
        help.setPrefix(prefix);
        return commandManager;
    }
}
