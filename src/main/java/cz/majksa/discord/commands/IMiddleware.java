package cz.majksa.discord.commands;

import java.util.function.Supplier;

public interface IMiddleware {
    Response run(Request request, Supplier<Response> next);
}
