package cz.majksa.discord.commands;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Contains all information about a command to be run in a by discord bot
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface Command {
    /**
     * @return the command itself
     */
    String command();

    /**
     * @return the description of the command
     */
    String description() default "";

    /**
     * @return the usage of the command
     */
    String usage() default "";

    /**
     * @return is command can be called with arguments
     */
    boolean acceptsArgs() default false;

    /**
     * @return command aliases
     */
    String[] aliases() default {};
}
