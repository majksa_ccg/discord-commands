package cz.majksa.discord.commands;

import cz.majksa.discord.commands.exception.RuntimeException;
import lombok.Getter;
import lombok.Setter;
import net.dv8tion.jda.api.EmbedBuilder;

import static cz.majksa.discord.commands.CommandsBuilder.LOGGER;

/**
 * Gives help about commands.
 */
@Getter
@Setter
@Command(command = "help", description = "Describes all commands.", usage = "help", acceptsArgs = true)
public class Help extends AbstractCommand {
    private String prefix;
    private CommandContainer commands = new CommandContainer();

    /**
     * Handles help command.
     *
     * @param request Parameters of the command.
     * @return {@link EmbedBuilder} response
     * @throws RuntimeException When help command is missing.
     */
    @Override
    public Response onCommand(Request request) throws RuntimeException {
        final String[] args = request.getArgs();
        final Response response = Response.from(request);
        AbstractCommand command = getSelectedCommand(args);
        CommandContainer commands;

        if (command == null) {
            command = this;
            commands = this.commands;
        } else {
            commands = command.commands;
        }

        if (commands.getCommandMap().size() == 0) {
            LOGGER.debug("[HELP] Showing details of command.");
            Command annotation = command.getClass().getAnnotation(Command.class);
            response.getEmbedBuilder()
                    .setDescription(annotation.description())
                    .addField("Usage", annotation.usage(), false);
        } else {
            LOGGER.debug("[HELP] Showing list of commands.");
            response.getEmbedBuilder().setDescription("**Current prefix: **" + prefix + String.join(" ", args));
            commands.getCommandMap().forEach((cls, cmd) -> {
                Command annotation = cls.getAnnotation(Command.class);
                response.getEmbedBuilder().addField(annotation.usage(), annotation.description(), false);
            });
        }

        return response;
    }

    protected AbstractCommand getSelectedCommand(String[] args) {
        CommandContainer commands = this.commands;
        AbstractCommand command = null;
        LOGGER.debug(String.join(" ", args));

        for (String arg: args) {
            LOGGER.debug("[HELP] Current argument: " + arg);
            if (!commands.containsKey(arg)) {
                throw new RuntimeException(String.format("Help for **%s** doesn't exist.", arg));
            }

            command = commands.get(arg);
            commands = command.commands;
        }

        return command;
    }
}
