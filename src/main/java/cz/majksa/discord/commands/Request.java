package cz.majksa.discord.commands;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import static cz.majksa.discord.commands.CommandsBuilder.LOGGER;

/**
 * Contains command parameters
 */
@Data
@RequiredArgsConstructor(staticName = "from")
public class Request {
    private final Guild guild;
    private final JDA jda;
    private final Member member;
    private final User user;
    private final TextChannel textChannel;
    private final Message getOriginalMessage;
    private final GuildMessageReceivedEvent event;
    private final AbstractCommand command;
    private final String[] args;

    /**
     * Creates {@link Request} from {@link #event} and {@link #args}.
     *
     * @param event {@link #event}
     * @param args {@link #args}
     * @return {@link Request}
     */
    public static Request from(GuildMessageReceivedEvent event, AbstractCommand command, String[] args) {
        LOGGER.debug("Preparing Request");
        return new Request(
            event.getGuild(), event.getJDA(), event.getMember(), event.getAuthor(), event.getChannel(), event.getMessage(), event,
            command, args
        );
    }
}
