package cz.majksa.discord.commands;

import cz.majksa.discord.commands.exception.CommandNotFoundException;
import cz.majksa.discord.commands.exception.RuntimeException;
import cz.majksa.discord.commands.exception.SubcommandNotFoundException;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;


/**
 * Handles and stores commands.
 */
@Data
public class CommandManager {
    private final CommandContainer commands;
    private final MiddlewareContainer middlewares;
    private final Help help;
    private final List<String> helpRoutes = new LinkedList<>();

    public CommandManager(CommandContainer commands, MiddlewareContainer middlewares, Help help) {
        this.commands = commands;
        this.middlewares = middlewares;
        this.help = help;
        final Command annotation = help.getClass().getAnnotation(Command.class);
        helpRoutes.add(annotation.command());
        helpRoutes.addAll(Arrays.asList(annotation.aliases()));
    }

    /**
     * This method will find the method corresponding to the input the user has
     * entered. If the command is valid it executes it otherwise this will print an
     * error message.
     *
     * @param event {@link GuildMessageReceivedEvent}
     * @param args Message arguments
     */
    public void handleMessage(GuildMessageReceivedEvent event, String[] args) {
        String commandName = args[0];
        String[] commandArguments = Arrays.copyOfRange(args, 1, args.length);

        handleCommand(commandName, commandArguments, event).send();
    }

    /**
     * This method goes threw all commands and finds the corresponding command.
     * Otherwise throws {@link CommandNotFoundException}.
     *
     * @param name Command name
     * @param args Message arguments
     * @param event {@link GuildMessageReceivedEvent}
     * @return {@link EmbedBuilder} response.
     */
    private Response handleCommand(String name, String[] args, GuildMessageReceivedEvent event) {
        try {
            if (helpRoutes.contains(name)) {
                return executeCommand(help, Request.from(event, help, args));
            }

            if (commands.containsKey(name)) {
                return handleCommand(event, args, commands.get(name));
            }
            Command closest = findClosest(name);
            if (closest == null) {
                throw new CommandNotFoundException(name);
            }
            throw new SubcommandNotFoundException(name, closest);
        } catch (RuntimeException e) {
            return Response.from(event, e);
        }
    }

    /**
     * This method goes threw all commands and finds the corresponding subcommand.
     * Otherwise throws {@link SubcommandNotFoundException}.
     *
     * @param event {@link GuildMessageReceivedEvent}
     * @param args Message arguments
     * @param command {@link AbstractCommand}
     * @return {@link EmbedBuilder} response.
     */
    private Response handleCommand(GuildMessageReceivedEvent event, String[] args, AbstractCommand command) {
        Command annotation = command.getClass().getAnnotation(Command.class);

        if (args.length != 0) {
            String commandName = args[0];
            String[] commandArguments = Arrays.copyOfRange(args, 1, args.length);
            CommandContainer commands = command.getCommands();

            if (commands.containsKey(commandName)) {
                return handleCommand(event, commandArguments, commands.get(commandName));
            }

            if (!annotation.acceptsArgs()) {
                Command closest = findClosest(commandName, commands);
                if (closest == null) {
                    throw new SubcommandNotFoundException(commandName);
                }
                throw new SubcommandNotFoundException(commandName, closest);
            }
        }

        return executeCommand(command, Request.from(event, command, args));

    }

    private Command findClosest(String name) {
        List<Command> commands = getCommands().getCommandMap().keySet().stream().map(cmd -> cmd.getAnnotation(Command.class)).collect(Collectors.toList());
        commands.add(help.getClass().getAnnotation(Command.class));
        return findClosest(name, commands);
    }

    private Command findClosest(String name, CommandContainer commandContainer) {
        return findClosest(name, commandContainer.getCommandMap().keySet().stream().map(cmd -> cmd.getAnnotation(Command.class)).collect(Collectors.toList()));
    }

    private Command findClosest(String name, List<Command> commands) {
        return CommandSimilarity.calculateClosestMatch(name, commands);
    }


    /**
     * Executes command with registered middlewares.
     *
     * @param command {@link AbstractCommand}
     * @param request {@link Request}
     * @return {@link Response}
     */
    private Response executeCommand(AbstractCommand command, Request request) {
        final ArrayDeque<Supplier<Response>> suppliers = new ArrayDeque<>();
        suppliers.add(() -> command.onCommand(request));

        middlewares.forEach((weight, middleware) -> {
            Supplier<Response> next = suppliers.getLast();
            suppliers.add(() -> middleware.run(request, next));
        });

        return suppliers.getLast().get();
    }
}
