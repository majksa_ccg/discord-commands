package cz.majksa.discord.commands;

import java.util.Collections;
import java.util.TreeMap;

/**
 * Contains middlewares.
 */
public class MiddlewareContainer extends TreeMap<Integer, IMiddleware> {
    public MiddlewareContainer() {
        super(Collections.reverseOrder());
    }

    /**
     * Adds {@link IMiddleware} to specified position.
     * Throws {@link IllegalArgumentException} if position is already occupied.
     *
     * @param position Position of middleware
     * @param middleware {@link IMiddleware}
     * @return {@link MiddlewareContainer}
     */
    public MiddlewareContainer register(Integer position, IMiddleware middleware) {
        if (containsKey(position)) {
            throw new IllegalArgumentException(String.format("Middleware with position %d is already registered.", position));
        }
        put(position, middleware);
        return this;
    }
}
