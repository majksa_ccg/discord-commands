package cz.majksa.discord.commands;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.similarity.LevenshteinDistance;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Made specifically for Majksa's discord commands library.
 * Requires org.apache.commons.lang3 and org.apache.commons.text!
 *
 * @author TOTHTOMI
 * @version 1.0
 */
public class CommandSimilarity {

    /**
     * Returns the closest match for an entered input.
     * <b>Input has to be the raw message from the event, so nothing splitted etc.</b>
     * Ideally used when a user misspells a command, and this would return closest found command.
     * Ideally you would print out something like: "Did you mean %command%?"
     *
     * @param input    the raw input
     * @param commands the list of registered commands
     * @return the closest match or null in case there's nothing close enough.
     */
    public static Command calculateClosestMatch(String input, List<Command> commands) {
        TreeMap<Double, Command> solutions = getSolutions(input, commands);

        if (solutions.size() > 0) {
            Map.Entry<Double, Command> highestPossibility = solutions.pollLastEntry();
            return highestPossibility.getValue();
        }

        return null;
    }

    /**
     * Populates our treeMap so we can get the highest possibility.
     * This will calculate a value for each individual command registered, and puts them into the map.
     * Closest match can be found using {@link TreeMap#pollLastEntry()}
     *
     * @param input    the input
     * @param commands the list of registered commands
     */
    private static TreeMap<Double, Command> getSolutions(String input, List<Command> commands) {
        TreeMap<Double, Command> solutions = new TreeMap<>();
        LevenshteinDistance levenshteinDistance = new LevenshteinDistance();
        for (Command command : commands) {
//            final String[] shortestArguments = new String[command.usage().split(" ").length];
//            System.arraycopy(inputSplitted, 0, shortestArguments, 0, Math.min(inputSplitted.length, shortestArguments.length));
//            String cmdArgs = StringUtils.join(shortestArguments, " ");
            int distance = levenshteinDistance.apply(input, command.command());
            solutions.put((1D - (distance / (Math.max(input.length(), command.command().length()) * 1D))) * 100D, command);
        }
        return solutions;
    }
}

