package cz.majksa.discord.commands.exception;


import cz.majksa.discord.commands.AbstractCommand;
import cz.majksa.discord.commands.Command;

/**
 * If class doesn't have @Command annotation
 */
public class AnnotationNotFoundException extends ClassNotFoundException {
    public AnnotationNotFoundException(AbstractCommand command) {
        super(String.format("Command %s must be annotated with %s.", command.getClass().getName(), Command.class.getName()));
    }
}
