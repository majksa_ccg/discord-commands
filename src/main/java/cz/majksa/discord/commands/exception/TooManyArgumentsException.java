package cz.majksa.discord.commands.exception;


/**
 * When too many arguments are given.
 */
public class TooManyArgumentsException extends RuntimeException {
    public TooManyArgumentsException() {
        super("Too many arguments.");
    }
}
