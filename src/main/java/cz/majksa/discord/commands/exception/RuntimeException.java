package cz.majksa.discord.commands.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * General exception when executing commands. Gets caught.
 */
@AllArgsConstructor
public class RuntimeException extends java.lang.RuntimeException {
    @Getter
    String message;
}
