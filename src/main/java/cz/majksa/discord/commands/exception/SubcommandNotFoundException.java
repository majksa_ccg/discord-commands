package cz.majksa.discord.commands.exception;


import cz.majksa.discord.commands.Command;

/**
 * Thrown is called command doesn't exist.
 */
public class SubcommandNotFoundException extends RuntimeException {
    public SubcommandNotFoundException(String command) {
        super(String.format("Subcommand **%s** was not found!", command));
    }

    public SubcommandNotFoundException(String command, Command suggestion) {
        super(String.format("Subcommand **%s** was not found! Did you mean\n`%s`?", command, suggestion.usage()));
    }
}
