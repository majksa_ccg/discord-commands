package cz.majksa.discord.commands.exception;


/**
 * When subcommand is not chosen.
 */
public class SubcommandNotChosenException extends RuntimeException {
    public SubcommandNotChosenException() {
        super("Please choose a subcommand.");
    }
}
