package cz.majksa.discord.commands.exception;


/**
 * Thrown if command with the same name is already registered.
 */
public class CommandExistsException extends IllegalArgumentException {
    public CommandExistsException(String command) {
        super(String.format("Command with name %s is already registered!", command));
    }
}
