package cz.majksa.discord.commands.exception;


/**
 * When too few arguments are given.
 */
public class TooFewArgumentsException extends RuntimeException {
    public TooFewArgumentsException() {
        super("Too few arguments.");
    }
}
