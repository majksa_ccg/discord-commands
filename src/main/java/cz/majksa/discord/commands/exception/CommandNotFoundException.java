package cz.majksa.discord.commands.exception;


import cz.majksa.discord.commands.Command;

/**
 * Thrown is called command doesn't exist.
 */
public class CommandNotFoundException extends RuntimeException {

    public CommandNotFoundException(String command) {
        super(String.format("Command **%s** was not found!", command));
    }

    public CommandNotFoundException(String command, Command suggestion) {
        super(String.format("Command **%s** was not found! Did you mean\n`%s`?", command, suggestion.usage()));
    }
}
