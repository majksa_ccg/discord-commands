package cz.majksa.discord.commands;

import cz.majksa.discord.commands.exception.RuntimeException;
import lombok.Data;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GenericGuildMessageEvent;
import org.apache.commons.text.WordUtils;

import javax.annotation.Nullable;
import java.awt.*;
import java.util.Date;
import java.util.function.Consumer;

import static cz.majksa.discord.commands.CommandsBuilder.LOGGER;

/**
 * Contains command response
 */
@Data
public class Response {
    private final EmbedBuilder embedBuilder = new EmbedBuilder();
    private final StringBuilder stringBuilder = new StringBuilder();
    private boolean send = true;
    private MessageChannel channel;

    public Response(MessageChannel channel) {
        this.channel = channel;
    }

    /**
     * Builds message
     *
     * @return Message to be sent.
     */
    public Message build() {
        return new MessageBuilder()
                .append(stringBuilder.toString())
                .setEmbed(embedBuilder.build())
            .build();
    }

    /**
     * Sends message to set channel.
     */
    public void send() {
        if (send) {
            LOGGER.debug(String.format("Sending message to %s", channel.getId()));
            channel.sendMessage(build()).queue();
        } else {
            LOGGER.debug("Send is set to false, message is not being send.");
        }
    }

    /**
     * Sends message to set channel.
     */
    public void send(@Nullable Consumer<? super Message> success) {
        if (send) {
            LOGGER.debug(String.format("Sending message to %s", channel.getId()));
            channel.sendMessage(build()).queue(success);
        } else {
            LOGGER.debug("Send is set to false, message is not being send.");
        }
    }

    /**
     * Sends message to set channel.
     */
    public void send(@Nullable Consumer<? super Message> success, @Nullable Consumer<? super Throwable> failure) {
        if (send) {
            LOGGER.debug(String.format("Sending message to %s", channel.getId()));
            channel.sendMessage(build()).queue(success, failure);
        } else {
            LOGGER.debug("Send is set to false, message is not being send.");
        }
    }

    /**
     * @param event {@link GenericGuildMessageEvent}
     * @return {@link Response}
     */
    public static Response from(GenericGuildMessageEvent event) {
        Response response = new Response(event.getChannel());
        response.getEmbedBuilder()
                .setTimestamp(new Date().toInstant())
                .setFooter(event.getJDA().getSelfUser().getName(), event.getJDA().getSelfUser().getAvatarUrl());
        return response;
    }

    /**
     *
     * @param request {@link Request}
     * @return {@link Response}
     */
    public static Response from(Request request) {
        return from(request, true);
    }

    /**
     *
     * @param request {@link Request}
     * @return {@link Response}
     */
    public static Response from(Request request, boolean send) {
        Response response = from(request.getEvent());
        response.getEmbedBuilder().setTitle(WordUtils.capitalize(request.getCommand().getClass().getAnnotation(Command.class).command()));
        response.send = send;
        return response;
    }

    /**
     * @param event {@link GenericGuildMessageEvent}
     * @param e {@link RuntimeException}
     * @return {@link Response}
     */
    public static Response from(GenericGuildMessageEvent event, RuntimeException e) {
        Response response = from(event);
        response.getEmbedBuilder().setTitle("Error").setColor(Color.RED).setDescription(e.getMessage());
        return response;
    }
}
