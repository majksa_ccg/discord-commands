package cz.majksa.discord.commands;

import cz.majksa.discord.commands.CommandManager;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

import static cz.majksa.discord.commands.CommandsBuilder.LOGGER;


/**
 * Controller, detects new messages and forwards them to command handler.
 */
@AllArgsConstructor
public class MessageReceivedListener extends ListenerAdapter {
    private final String prefix;
    private final CommandManager commandManager;

    /**
     * Called when a new message in guild is sent.
     * @param event {@link GuildMessageReceivedEvent} from JDA
     */
    @Override
    public void onGuildMessageReceived(@Nonnull GuildMessageReceivedEvent event) {
        String input = event.getMessage().getContentRaw();
        if (!input.startsWith(prefix) || input.length() == prefix.length()) return;
        LOGGER.debug(String.format("Input received: %s", input));

        String[] args = input.substring(prefix.length()).split(" ");
        if (args.length == 0) {
            return;
        }

        commandManager.handleMessage(event, args);
    }
}
