package cz.majksa.discord.commands;

import cz.majksa.discord.commands.exception.AnnotationNotFoundException;
import cz.majksa.discord.commands.exception.CommandExistsException;
import lombok.Getter;
import lombok.SneakyThrows;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Container for {@link AbstractCommand}
 */
@Getter
public class CommandContainer {
    private final Map<Class<? extends AbstractCommand>, AbstractCommand> commandMap = new LinkedHashMap<>();
    private final Map<String, Class<? extends AbstractCommand>> routes = new LinkedHashMap<>();

    /**
     * This method will register provided commands.
     *
     * @param commands {@link AbstractCommand} list of commands to be registered
     */
    public void add(AbstractCommand... commands) throws AnnotationNotFoundException {
        for (AbstractCommand command: commands) {
            Class<? extends AbstractCommand> cls = command.getClass();
            Command annotation = cls.getAnnotation(Command.class);
            if (annotation == null) {
                throw new AnnotationNotFoundException(command);
            }
            commandMap.putIfAbsent(cls, command);
            try {
                routes.putIfAbsent(annotation.command(), cls);
                for (String alias : annotation.aliases()) {
                    routes.putIfAbsent(alias, cls);
                }
            } catch (IllegalArgumentException e) {
                throw new CommandExistsException(annotation.command());
            }
        }
    }

    public AbstractCommand get(String route) {
        return commandMap.get(routes.get(route));
    }

    public boolean containsKey(String route) {
        return routes.containsKey(route);
    }
}
