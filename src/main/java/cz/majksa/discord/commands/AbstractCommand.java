package cz.majksa.discord.commands;


import cz.majksa.discord.commands.exception.RuntimeException;
import lombok.Getter;


/**
 * Abstract Command class with commands manager and default method.
 */
public abstract class AbstractCommand {
    @Getter
    protected CommandContainer commands = new CommandContainer();

    /**
     * Method that is executed on command call.
     *
     * @param request Request of the command.
     * @return {@link Response} Response of the command.
     */
    public abstract Response onCommand(Request request) throws RuntimeException;
}
