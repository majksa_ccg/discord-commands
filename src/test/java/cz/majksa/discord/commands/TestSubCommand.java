package cz.majksa.discord.commands;

import cz.majksa.discord.commands.exception.RuntimeException;

@Command(command = "subcommand", aliases = {"s","sc"})
class TestSubCommand extends AbstractCommand {

    @Override
    public Response onCommand(Request request) throws RuntimeException {
        return Response.from(request);
    }
}
