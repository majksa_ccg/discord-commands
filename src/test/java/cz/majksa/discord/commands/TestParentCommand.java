package cz.majksa.discord.commands;


import cz.majksa.discord.commands.exception.RuntimeException;
import lombok.SneakyThrows;

@Command(command = "command", aliases = {"c","co"})
class TestParentCommand extends AbstractCommand {

    @SneakyThrows
    public TestParentCommand() {
        commands.add(new TestSubCommand());
    }

    @Override
    public Response onCommand(Request request) throws RuntimeException {
        return Response.from(request);
    }
}
