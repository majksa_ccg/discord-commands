package cz.majksa.discord.commands;

import cz.majksa.discord.commands.exception.AnnotationNotFoundException;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CommandContainerTest {
    private static final CommandContainer CONTAINER = new CommandContainer();
    private static final AbstractCommand COMMAND = new TestParentCommand();
    private static final AbstractCommand COMMAND2 = new TestParentCommand2();

    static {
        try {
            CONTAINER.add(COMMAND, COMMAND2);
        } catch (AnnotationNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testContainsRoutes() {
        assertTrue(CONTAINER.containsKey("c"));
        assertTrue(CONTAINER.containsKey("co"));
        assertTrue(CONTAINER.containsKey("command"));
        assertTrue(CONTAINER.containsKey("c2"));
        assertTrue(CONTAINER.containsKey("co2"));
        assertTrue(CONTAINER.containsKey("command2"));
        assertTrue(COMMAND.commands.containsKey("s"));
        assertTrue(COMMAND.commands.containsKey("sc"));
        assertTrue(COMMAND.commands.containsKey("subcommand"));
    }

    @Test
    public void testMappingCommand() {
        assertEquals(COMMAND, CONTAINER.get("c"));
        assertEquals(COMMAND, CONTAINER.get("co"));
        assertEquals(COMMAND, CONTAINER.get("command"));
        assertEquals(COMMAND2, CONTAINER.get("c2"));
        assertEquals(COMMAND2, CONTAINER.get("co2"));
        assertEquals(COMMAND2, CONTAINER.get("command2"));
    }

    @Test
    public void testCommandContainerSizes() {
        assertEquals(2, CONTAINER.getCommandMap().size());
        assertEquals(6, CONTAINER.getRoutes().size());
        assertEquals(1, COMMAND.commands.getCommandMap().size());
        assertEquals(3, COMMAND.commands.getRoutes().size());
    }
}
