package cz.majksa.discord.commands;


import cz.majksa.discord.commands.exception.RuntimeException;

@Command(command = "command2", aliases = {"c2","co2"})
class TestParentCommand2 extends AbstractCommand {

    @Override
    public Response onCommand(Request request) throws RuntimeException {
        return Response.from(request);
    }
}
