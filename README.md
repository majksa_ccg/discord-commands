# Discord Commands <a href="#">

Discord command framework for Java, supporting [JDA](https://github.com/DV8FromTheWorld/JDA). It helps you creating commands within seconds in a clean and simple way.
It also supports middlewares.

## Summary
1. [Installation](#installation)
2. [Setup](#setup)
3. [Creating a command](#creating-a-command)
4. [Creating a subcommand](#creating-a-subcommand)
5. [Creating a middleware](#creating-a-middleware)
6. [Throwing exceptions](#throwing-exceptions)
7. [Command response](#command-response)
8. [Customizing Help command](#customizing-help-command)

## Installation
Make sure to replace `%version%` with the latest version number or a commit hash, e.g. `2.0.1`.
More info [HERE](https://jitpack.io/#com.gitlab.majksa_ccg/discord-commands)

###  Maven
```xml
<repository>
  <id>jitpack.io</id>
  <url>https://jitpack.io</url>
</repository>
...
<dependency>
  <groupId>com.gitlab.majksa_ccg</groupId>
  <artifactId>discord-commands</artifactId>
  <version>%version%</version>
</dependency>
```
###  Gradle
```gradle
repositories {
    maven { url 'https://jitpack.io' }
}

dependencies {
    implementation 'com.gitlab.majksa_ccg:discord-commands:%version%'
}
```

## Setup
* create `CommandsBuilder` with an instance of `Help` and `commands prefix`
* register commands - more info [HERE](#creating-a-command)
* register middlewares - more info [HERE](#creating-a-middleware)
* build `CommandsBuilder` with JDA or JDABuilder as it's only parameter.
```java
public class Main {

    public static void main(String[] args) {
        CommandsBuilder commandsBuilder = new CommandsBuilder(new Help(), "!")
            // Register commands
            .registerCommands(/* will be explained in Commands section.*/)
            // Register middlewares
            .registerMiddleware(/* will be explained in Middlewares section.*/)
            // Build
            .build(jda);
    }
}
```

## Creating a command
Command **must** extends `AbstractCommand` and be annotated with `Command`.
```java
@Command(command = "id", usage = "id", description = "Prints ID of current user.")
public class Id extends AbstractCommand {

    @Override
    public Response onCommand(Request request) {
        Response response = Response.from(request);
        response.getEmbedBuilder().setDescription(request.getUser().getAsMention() + ": " + request.getUser().getId());
        return response;
    }
}
```
Now you need to register the command in command manager created in [Setup](#setup) by giving and instance of the command to `add` method.
```java
commandsBuilder.registerCommands(new Id());
```

### Creating a subcommand
Creating a subcommand is the same as creating a command, but instead of registering the command in [Setup](#setup) stage, you will register it in constructor of parent command.
Every command has a `CommandContainer commands` attribute, where you can register subcommands.
```java
@Command(command = "account", usage = "account", description = "Account management.")
public class Account extends AbstractCommand {

    public Account() {
        commands.add(new Open());
    }
}
```
```java
@Command(command = "open", usage = "open [user-id]", description = "Opens an account for the specified discord user")
public class Open extends AbstractCommand {

    @Override
    public Response onCommand(Request request) {
        Member member = request.getMember();
        // User registration logic
        Response response = Response.from(request);
        response.getEmbedBuilder()
            .setDescription("Success")
            .setColor(Color.GREEN);
        return response;
    }
}
```

## Creating a middleware
Middleware must implement `IMiddleware`.
```java
public class LoggingMiddleware implements IMiddleware {
    private Logger logger;

    public LoggingMiddleware(Logger logger) {
        this.logger = logger;
    }

    @Override
    public Response run(Request request, Supplier<Response> next) {
        logger.info(request);
        Response response = next.get();
        logger.info(response);
        return response;
    }
}
```
Now you need to register the middleware in command manager created in [Setup](#setup) with a position.
```java
commandsBuilder.registerMiddleware(100, new LoggingMiddleware(LOGGER));
```

## Throwing exceptions
You can throw exceptions in a Command or a Middleware and it will be turned into a nice embed, with message of the exception.
Exception must extend `exception.RuntimeException`.
Exceptions ready to be used:
* RuntimeException
* SubcommandNotChosenException
* TooManyArgumentsException
* TooFewArgumentsException

## Command response
Every `onCommand` method must return an instance of `Respose`. You can use `Response.from(request)` to generate default response.
If you want to send multiple responses, you can send them by calling method `send` on an instance of `Response`.
If you don't want to send any response, you can do so, by calling `setSend(false)` on an instance of `Response`.

## Customizing Help command
To customize the Help command, you need to extend the `onCommand` method.
Returned response is the Help embed.
All registered commands are stored in `CommandContainer commands`. 
